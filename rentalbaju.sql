-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Okt 2019 pada 10.08
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentalbaju`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(20) NOT NULL,
  `nama` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` float NOT NULL,
  `no_ktp` decimal(20,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id_customer`, `nama`, `username`, `password`, `alamat`, `no_telp`, `no_ktp`) VALUES
(1, 'Andini', 'andini', '1234', 'Gresik', 877, '352511');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_peminjaman`
--

CREATE TABLE `detail_peminjaman` (
  `id_detail_peminjaman` int(3) NOT NULL,
  `id_customer` int(3) NOT NULL,
  `id_item` int(20) NOT NULL,
  `id_peminjaman` int(20) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `batas_kembali` date NOT NULL,
  `jumlah` int(20) NOT NULL,
  `status` varchar(20) NOT NULL COMMENT 'PENDING, REQUEST, DIPINJAM, DIKEMBALIKAN'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_peminjaman`
--

INSERT INTO `detail_peminjaman` (`id_detail_peminjaman`, `id_customer`, `id_item`, `id_peminjaman`, `tgl_pinjam`, `batas_kembali`, `jumlah`, `status`) VALUES
(1, 1, 1, 1, '2019-09-11', '2019-09-19', 1, 'DIPINJAM'),
(2, 1, 2, 1, '2019-09-11', '2019-09-19', 1, 'DIPINJAM'),
(3, 1, 3, 1, '2019-09-11', '2019-09-30', 1, 'DIKEMBALIKAN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `item`
--

CREATE TABLE `item` (
  `id_item` int(3) NOT NULL,
  `id_jenis` int(3) NOT NULL,
  `id_ukuran` int(3) NOT NULL,
  `nama_item` varchar(20) NOT NULL,
  `nama_daerah` varchar(20) NOT NULL,
  `harga` int(3) NOT NULL,
  `stok` int(3) NOT NULL,
  `gambar` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `item`
--

INSERT INTO `item` (`id_item`, `id_jenis`, `id_ukuran`, `nama_item`, `nama_daerah`, `harga`, `stok`, `gambar`) VALUES
(1, 1, 1, 'Remo', 'Jawa Timur', 80000, 10, 'remo_baju.jpg'),
(2, 2, 1, 'Remo', 'Jawa Timur', 40000, 10, 'remo_aksesoris.jpg'),
(3, 1, 15, 'Sunda', 'Jawa Barat', 90000, 14, 'sunda.jpg'),
(4, 2, 15, 'Sunda', 'Jawa Barat', 30000, 14, 'sunda_aksesoris.jpg'),
(5, 1, 2, 'Remo', 'BB', 1, 2, 'gambar.jpg'),
(9, 2, 2, 'Remo', 'BB', 25000, 2, 'gambar.jpg'),
(11, 1, 1, 'Remo', 'Jawa Tengah', 60000, 15, 'remo-jateng.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'PAKAIAN'),
(2, 'AKSESORIS');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(20) NOT NULL,
  `id_users` int(20) NOT NULL,
  `id_customer` int(20) NOT NULL,
  `total_harga` int(20) NOT NULL,
  `status` varchar(20) NOT NULL COMMENT 'PENDING, REQUEST, CONFIRM'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `id_users`, `id_customer`, `total_harga`, `status`) VALUES
(1, 2, 1, 210000, 'CONFIRM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengembalian`
--

CREATE TABLE `pengembalian` (
  `id_pengembalian` int(20) NOT NULL,
  `id_detail_peminjaman` int(20) NOT NULL,
  `tgl_kembali` date NOT NULL,
  `denda` decimal(20,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengembalian`
--

INSERT INTO `pengembalian` (`id_pengembalian`, `id_detail_peminjaman`, `tgl_kembali`, `denda`) VALUES
(1, 3, '2019-09-28', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ukuran`
--

CREATE TABLE `ukuran` (
  `id_ukuran` int(11) NOT NULL,
  `kelamin` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ukuran` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `usia` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ukuran`
--

INSERT INTO `ukuran` (`id_ukuran`, `kelamin`, `ukuran`, `usia`) VALUES
(1, 'LAKI-LAKI', 'S', 'DEWASA'),
(2, 'LAKI-LAKI', 'M', 'DEWASA'),
(3, 'LAKI-LAKI', 'L', 'DEWASA'),
(4, 'LAKI-LAKI', 'XL', 'DEWASA'),
(5, 'PEREMPUAN', 'S', 'DEWASA'),
(6, 'PEREMPUAN', 'M', 'DEWASA'),
(7, 'PEREMPUAN', 'L', 'DEWASA'),
(8, 'PEREMPUAN', 'XL', 'DEWASA'),
(9, 'LAKI-LAKI', 'S', 'ANAK-ANAK'),
(10, 'LAKI-LAKI', 'M', 'ANAK-ANAK'),
(11, 'LAKI-LAKI', 'L', 'ANAK-ANAK'),
(12, 'LAKI-LAKI', 'XL', 'ANAK-ANAK'),
(13, 'PEREMPUAN', 'S', 'ANAK-ANAK'),
(14, 'PEREMPUAN', 'M', 'ANAK-ANAK'),
(15, 'PEREMPUAN', 'L', 'ANAK-ANAK'),
(16, 'PEREMPUAN', 'XL', 'ANAK-ANAK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(10) NOT NULL,
  `nama` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` decimal(20,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `nama`, `username`, `password`, `status`, `no_telp`) VALUES
(1, 'aldhi', 'aldhinya', '1221', 'ADMIN', '812345'),
(2, 'Moeslim', 'moeslim', '123', 'KASIR', '812345');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indeks untuk tabel `detail_peminjaman`
--
ALTER TABLE `detail_peminjaman`
  ADD PRIMARY KEY (`id_detail_peminjaman`),
  ADD KEY `id_item` (`id_item`),
  ADD KEY `id_peminjaman` (`id_peminjaman`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indeks untuk tabel `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id_item`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_ukuran` (`id_ukuran`);

--
-- Indeks untuk tabel `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_users` (`id_users`);

--
-- Indeks untuk tabel `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD PRIMARY KEY (`id_pengembalian`),
  ADD KEY `id_detail_peminjaman` (`id_detail_peminjaman`);

--
-- Indeks untuk tabel `ukuran`
--
ALTER TABLE `ukuran`
  ADD PRIMARY KEY (`id_ukuran`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `detail_peminjaman`
--
ALTER TABLE `detail_peminjaman`
  MODIFY `id_detail_peminjaman` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `item`
--
ALTER TABLE `item`
  MODIFY `id_item` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pengembalian`
--
ALTER TABLE `pengembalian`
  MODIFY `id_pengembalian` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `ukuran`
--
ALTER TABLE `ukuran`
  MODIFY `id_ukuran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_peminjaman`
--
ALTER TABLE `detail_peminjaman`
  ADD CONSTRAINT `fk_id_customer2` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`),
  ADD CONSTRAINT `fk_id_item` FOREIGN KEY (`id_item`) REFERENCES `item` (`id_item`),
  ADD CONSTRAINT `fk_id_peminjaman` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`);

--
-- Ketidakleluasaan untuk tabel `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `fk_id_jenis` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  ADD CONSTRAINT `fk_id_ukuran` FOREIGN KEY (`id_ukuran`) REFERENCES `ukuran` (`id_ukuran`);

--
-- Ketidakleluasaan untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `fk_id_customer` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`),
  ADD CONSTRAINT `fk_id_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD CONSTRAINT `fk_id_detail_peminjaman` FOREIGN KEY (`id_detail_peminjaman`) REFERENCES `detail_peminjaman` (`id_detail_peminjaman`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
