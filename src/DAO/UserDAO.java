/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import model.UserModel;

/**
 *
 * @author ALDHINYA
 */
public interface UserDAO {
    public boolean insert(UserModel p);
    
    public boolean update(UserModel p);
    
    public boolean delete(UserModel p);
    
    public List<UserModel> getAllUser();
    
    public UserModel getByID(String id);
    
    public List<UserModel> getByName(String name);
}
