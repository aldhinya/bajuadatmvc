/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import koneksi.Koneksi;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.UserModel;

/**
 *
 * @author ALDHINYA
 */
public class UserDAOMysql implements UserDAO{
   @Override
    public boolean insert(UserModel p) {
        String sql = "INSERT INTO users values (null, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement statement = Koneksi.getConnection().prepareStatement(sql);
            
            statement.setString(1, p.getNama());
            statement.setString(2, p.getUsername());
            statement.setString(3, p.getPassword());
            statement.setString(4, p.getStatus());
            statement.setString(5, p.getNoTelp());
            
            int row = statement.executeUpdate();
            if (row > 0) {
                return true;
            }
            statement.close();
        } catch (Exception ex) {
            Logger.getLogger(UserModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean update(UserModel p) {
        String sql = "UPDATE users SET nama='"+p.getNama()+"' , username='"+p.getNama()+"' , password='"+p.getUsername()+"', status='"+p.getPassword()+"', no_telp='"+p.getNoTelp()+"' where id_user="+p.getId()+"";
        try {
            PreparedStatement statement = Koneksi.getConnection().prepareStatement(sql);
            
            int row = statement.executeUpdate();
            if (row > 0) {
                return true;
            }
            
            statement.close();
        } catch (Exception ex) {
            Logger.getLogger(UserModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete(UserModel p) {
       String sql = "DELETE from users where id_user="+p.getId()+"";
        try {
            PreparedStatement statement = Koneksi.getConnection().prepareStatement(sql);
            
            int row = statement.executeUpdate();
            if (row > 0) {
                return true;
            }
            
            statement.close();
        } catch (Exception ex) {
            Logger.getLogger(UserModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public List<UserModel> getAllUser() {
        List<UserModel> user = new ArrayList<UserModel>();
        String sql = "Select * from users;";
        try {
            if (Koneksi.getConnection()==null){
                return null;
            }else{
                PreparedStatement statement = Koneksi.getConnection().prepareStatement(sql);

                ResultSet rs = statement.executeQuery();
                while (rs.next()){
                    UserModel p = new UserModel(
                            rs.getString(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getString(6)
                    );
                    user.add(p);
                }
                statement.close();
            }
        } catch (Exception ex) {
            Logger.getLogger(UserModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    @Override
    public UserModel getByID(String id) {
        return null;
    }

    @Override
    public List<UserModel> getByName(String name) {
        return null;
    }
}
