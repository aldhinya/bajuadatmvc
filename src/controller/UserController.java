/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.UserDAO;
import DAO.UserDAOMysql;
import java.util.List;
import model.CrudState;
import model.UserModel;
import java.util.Observable;
/**
 *
 * @author ALDHINYA
 */
public class UserController extends Observable{
    private UserDAO dao = new UserDAOMysql();
    private CrudState crudState;
    
    public void setDAO(UserDAO d){
        dao = d;
    }
    
    public void manipulate(UserModel p, CrudState c){
        boolean result = false;
        this.crudState = c;
        
        switch (c){
            case INSERT:
                result = dao.insert(p);
                break;
            case UPDATE:
                result = dao.update(p);
                break;
            case DELETE:
                result = dao.delete(p);
                break;
        }
        
        setChanged();
        if (result){
            notifyObservers(p);
        }else{
            notifyObservers();
        }
    }
    
    public CrudState getCrudState(){
        return crudState;
    }
    
    public List<UserModel> getAllUser() {
        return dao.getAllUser();
    }
}
